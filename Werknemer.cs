﻿using System;

namespace Vervloessem_Wout_oef24._3
{
    class Werknemer
    {
        // weet niet waarom ik ze moest in comment zetten maar anders gaf het errors.


        //private string _naam;
        //private string _voornaam;
        //private double _verdiensten;

        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public double Verdiensten { get; set; }
        public Werknemer(string naam, string voornaam, double verdiensten)
        {
            Naam = naam;
            Voornaam = voornaam;
            Verdiensten = verdiensten;
        }
        public string VolledigeWeergave
        {
            get
            {


                return Naam + "           " + Voornaam + "           " + Verdiensten+Environment.NewLine;

            }

        }

    }
}
